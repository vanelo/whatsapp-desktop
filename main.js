const electron = require('electron');
const {app, BrowserWindow} = electron;

app.on('ready', () => {
	let win = new BrowserWindow({
		width:800,
		height:600,
		icon:__dirname + '/assets/icons/png/48x48.png'
	});
	win.loadURL('http://web.whatsapp.com/');
	win.on('closed', function(){
		win = null;
		app.quit();
		console.log('whatsapp app is closed');
	});
});